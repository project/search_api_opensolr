(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.searchApiOpenSolrPassword = {
    attach: function (context, settings) {
      const defaultValue = settings.opensolr_key ? settings.opensolr_key : '';
      const $passwordInput = $(once('pass-shown', '.pass-show', context));
      $passwordInput.val(defaultValue);
      if ($passwordInput.length > 0) {
        $passwordInput.after('<span class="opensolr-toggle-password">' + Drupal.t('Show') + '</span>');
        $('.opensolr-toggle-password', context).on('click', function () {
          $(this).text($(this).text() === Drupal.t('Show') ? 'Hide' : 'Show');
          $(this).prev().attr('type', function (index, attr) {
            return attr === 'password' ? 'text' : 'password';
          });
        });
      }
    }
  };

})(jQuery, Drupal);
