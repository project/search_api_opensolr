<?php

namespace Drupal\search_api_opensolr\Traits;

/**
 * Opensolr trait for managing optional key module dependency.
 */
trait OpensolrKeyDependencyTrait {

  /**
   * Gets the module handler service.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   Returns the module handler service.
   */
  protected function moduleHandlerService() {
    return \Drupal::moduleHandler();
  }

  /**
   * Gets the key repository service if available.
   *
   * @return false|\Drupal\key\KeyRepositoryInterface
   *   Returns the key repository service if the module is installed, FALSE
   *   otherwise.
   */
  protected function keyRepositoryService() {
    if ($this->moduleHandlerService()->moduleExists('key')) {
      return \Drupal::service('key.repository');
    }
    return FALSE;
  }

}
