<?php

namespace Drupal\search_api_opensolr\OpenSolrApi\Components;

use Drupal\search_api_opensolr\OpenSolrApi\OpenSolrResponse;

/**
 * Provides an interface defining an OpenSolrAccount component.
 *
 * @package Drupal\search_api_opensolr\OpenSolrApi\Components
 */
interface OpenSolrAccountInterface {

  /**
   * Sends an email verification code to the given unregistered email.
   *
   * @param string $email
   *   The email.
   *
   * @return \Drupal\search_api_opensolr\OpenSolrApi\OpenSolrResponse
   *   Returns the opensolr response object.
   */
  public function sendEmailCode(string $email): OpenSolrResponse;

  /**
   * Creates an opensolr account.
   *
   * @param string $email
   *   The email.
   * @param string $code
   *   The verification code received by email.
   * @param string $password
   *   The password to set for the new account.
   *
   * @return \Drupal\search_api_opensolr\OpenSolrApi\OpenSolrResponse
   *   Returns the opensolr response object.
   */
  public function createAccount(string $email, string $code, string $password): OpenSolrResponse;

}
