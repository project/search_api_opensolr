<?php

namespace Drupal\search_api_opensolr\OpenSolrApi\Components;

use Drupal\search_api_opensolr\OpenSolrApi\OpenSolrBase;
use Drupal\search_api_opensolr\OpenSolrApi\OpenSolrResponse;

/**
 * Defines the opensolr account API component.
 *
 * @package Drupal\search_api_opensolr\OpenSolrApi\Components
 */
class OpenSolrAccount extends OpenSolrBase implements OpenSolrAccountInterface {

  const OPENSOLR_ENDPOINT_URL = 'https://opensolr.com/users';

  /**
   * {@inheritdoc}
   */
  public function sendEmailCode(string $email): OpenSolrResponse {
    $params['email'] = $email;
    return $this->apiCall('/send_email_code/api/v2', 'GET', $params, TRUE, FALSE, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function createAccount(string $email, string $code, string $password): OpenSolrResponse {
    $params = [
      'email' => $email,
      'code' => $code,
      'password' => $password,
    ];
    return $this->apiCall('/create_user/api/v2', 'GET', $params, TRUE, FALSE, FALSE);
  }

}
