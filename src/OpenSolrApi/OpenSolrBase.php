<?php

namespace Drupal\search_api_opensolr\OpenSolrApi;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api_opensolr\Services\OpenSolrConfig;
use Drupal\search_api_opensolr\Traits\OpensolrKeyDependencyTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * The core of the opensolr API calls.
 *
 * @package Drupal\search_api_opensolr\OpenSolrApi
 */
class OpenSolrBase {
  use OpensolrKeyDependencyTrait;
  use StringTranslationTrait;

  const OPENSOLR_ENDPOINT_URL = 'https://opensolr.com/solr_manager/api';

  /**
   * GuzzleHttp client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The OpenSolr API credentials as key valued array.
   *
   * @var array
   */
  protected $apiCredentials;

  /**
   * The JSON serializer service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $json;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  public function __construct(ClientInterface $http_client, Json $json, OpenSolrConfig $openSolrConfig, FileUrlGeneratorInterface $file_url_generator) {
    $this->httpClient = $http_client;
    $this->json = $json;
    $this->fileUrlGenerator = $file_url_generator;
    $this->apiCredentials = $openSolrConfig->getApiCredentials();
  }

  /**
   * General api call method.
   *
   * @param string $path
   *   The path of the api endpoint.
   * @param string $method
   *   The HTTP method. E.g. GET, POST, etc.
   * @param array $params
   *   An array of params to add to the request.
   * @param bool $returnObject
   *   A boolean indicating if we want the object as response or not.
   * @param bool $useMultipart
   *   A boolean indicating if we use multipart arguments or not.
   * @param bool $attachApiCredentials
   *   A boolean indicating if the stored API credentials should be
   *   automatically attached to the request or not. Defaults to TRUE.
   *
   * @return \Drupal\search_api_opensolr\OpenSolrApi\OpenSolrResponse|mixed|\Psr\Http\Message\ResponseInterface|null
   *   Returns the response from the API.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function apiCall(string $path, string $method = 'GET', array $params = [], bool $returnObject = FALSE, bool $useMultipart = FALSE, bool $attachApiCredentials = TRUE) {
    $options = [];
    if ($attachApiCredentials) {
      // Add API credentials.
      $params = array_merge($this->apiCredentials, $params);
    }
    if (!$useMultipart) {
      // Add necessary headers.
      $headers = [
        'Content-type' => 'application/json',
      ];
      $data = !empty($params) ? $this->json->encode($params) : NULL;
      // Build the request, including path and headers. Internal use.
      $options['headers'] = $headers;
      $options['body'] = $data;
    }
    else {
      $this->attachMultipart($options, $params);
    }
    $url = static::OPENSOLR_ENDPOINT_URL . $path;
    if ($method == 'GET' && !empty($params)) {
      $url .= '?' . UrlHelper::buildQuery($params);
    }
    try {
      $response = new OpenSolrResponse($this->httpClient->request($method, $url, $options));
      if ($returnObject) {
        return $response;
      }
      else {
        return $response->__get('data');
      }
    }
    catch (RequestException $e) {
      // RequestException gets thrown for any response status.
      $response = $e->getResponse();
      if (!isset($response) || !$response) {
        throw new OpenSolrException($response, $e->getMessage(), $e->getCode(), $e);
      }
    }

  }

  /**
   * Attaches the multipart values for requests that send files.
   *
   * @param array $options
   *   An array with the extra options for the API request.
   * @param array $params
   *   An array of params to add to the request.
   */
  protected function attachMultipart(array &$options, array $params) {
    $multipart = [];
    foreach ($params as $key => $value) {
      // If it's a file, we need to add the filename and create a url for it.
      if (file_exists($value)) {
        $multipart[] = [
          'name' => $key,
          'contents' => file_get_contents($value),
          'filename' => substr($value, strrpos($value, '/') + 1),
        ];
      }
      else {
        $multipart[] = [
          'name' => $key,
          'contents' => $value,
        ];
      }
    }
    $options['multipart'] = $multipart;
    $options['decode_content'] = FALSE;
  }

}
