<?php

namespace Drupal\search_api_opensolr\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Interface for the Multistep step instances.
 *
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepBase
 * @see \Drupal\search_api_opensolr\Form\Multistep\StepPrerequisites
 * @see \Drupal\search_api_opensolr\Form\Multistep\StepEmailVerification
 * @see \Drupal\search_api_opensolr\Form\Multistep\StepRegistration
 */
interface MultistepStepInterface {

  const EMAIL_MAX_LENGTH = 220;

  /**
   * Gets the step label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   A translatable string representing the label of the step.
   */
  public function label(): TranslatableMarkup;

  /**
   * Gets the position of the step in the multistep form.
   *
   * @return int
   *   The weight of the step.
   */
  public function weight(): int;

  /**
   * Gets the ID of the next step.
   *
   * @return string|bool
   *   Returns the ID of the next step if any, FALSE otherwise.
   */
  public function getNextStep(): bool|string;

  /**
   * Gets the ID of the previous step.
   *
   * @return string|bool
   *   Returns the ID of the previous step if any, FALSE otherwise.
   */
  public function getPreviousStep(): bool|string;

  /**
   * Builds the multistep step form.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function buildForm(array &$form, FormStateInterface $form_state);

  /**
   * Handles validation for the multistep step form.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state);

  /**
   * Handles submission for the multistep step form.
   *
   * @param array $form
   *   An associative array containing the initial structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state);

}
