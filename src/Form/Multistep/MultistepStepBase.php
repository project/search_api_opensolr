<?php

namespace Drupal\search_api_opensolr\Form\Multistep;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\search_api_opensolr\Form\GetStartedForm;
use Drupal\search_api_opensolr\OpenSolrApi\Components\OpenSolrAccountInterface;
use Drupal\search_api_opensolr\Services\OpenSolrConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for the Multistep step instances.
 *
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepInterface
 * @see \Drupal\search_api_opensolr\Form\Multistep\StepPrerequisites
 * @see \Drupal\search_api_opensolr\Form\Multistep\StepEmailVerification
 * @see \Drupal\search_api_opensolr\Form\Multistep\StepRegistration
 */
abstract class MultistepStepBase implements MultistepStepInterface, ContainerInjectionInterface {
  use DependencySerializationTrait;
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The private temp store for opensolr get started form.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $privateTempStore;

  /**
   * The opensolr API for account management.
   *
   * @var \Drupal\search_api_opensolr\OpenSolrApi\Components\OpenSolrAccountInterface
   */
  protected OpenSolrAccountInterface $openSolrAccount;

  /**
   * The opensolr config manager.
   *
   * @var \Drupal\search_api_opensolr\Services\OpenSolrConfig
   */
  protected OpenSolrConfig $openSolrConfig;

  public function __construct(PrivateTempStore $privateTempStore, OpenSolrAccountInterface $openSolrAccount, OpenSolrConfig $openSolrConfig) {
    $this->privateTempStore = $privateTempStore;
    $this->openSolrAccount = $openSolrAccount;
    $this->openSolrConfig = $openSolrConfig;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private')->get(GetStartedForm::TEMP_STORE),
      $container->get('search_api_opensolr.account'),
      $container->get('search_api_opensolr.config')
    );
  }

}
