<?php

namespace Drupal\search_api_opensolr\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\search_api_opensolr\OpenSolrErrors;

/**
 * Handles the Email Verification step in the multistep form.
 *
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepInterface
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepBase
 */
class StepEmailVerification extends MultistepStepBase {

  const ID = 'email_verification';

  /**
   * {@inheritdoc}
   */
  public function weight(): int {
    return 2;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Email Verification');
  }

  /**
   * {@inheritdoc}
   */
  public function getNextStep(): bool|string {
    return StepRegistration::ID;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousStep(): bool|string {
    return StepPrerequisites::ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Enter the email you wish to register to opensolr with'),
      '#maxlength' => MultistepStepInterface::EMAIL_MAX_LENGTH,
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo Do we verify if the email is the same when using back & next? Resend the code?
    $response = $this->openSolrAccount->sendEmailCode($form_state->getValue('email'));
    if (!$response->isSuccess()) {
      $form_state->setErrorByName('email', $this->t('Failed to send verification code with the following message: %message', [
        '%message' => OpenSolrErrors::getError($response->getResponseData()),
      ]));
    }
    else {
      $this->privateTempStore->set('email', $form_state->getValue('email'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus($this->t('An email has been sent to %email with a registration code.', [
      '%email' => $form_state->getValue('email'),
    ]));
    $this->privateTempStore->set('step', $this->getNextStep());
    $form_state->setRebuild();
  }

}
