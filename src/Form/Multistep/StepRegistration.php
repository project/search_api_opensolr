<?php

namespace Drupal\search_api_opensolr\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Password\DefaultPasswordGenerator;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\search_api_opensolr\OpenSolrErrors;

/**
 * Handles the Registration step in the multistep form.
 *
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepInterface
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepBase
 */
class StepRegistration extends MultistepStepBase {

  const ID = 'registration';

  /**
   * {@inheritdoc}
   */
  public function weight(): int {
    return 3;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Registration');
  }

  /**
   * {@inheritdoc}
   */
  public function getNextStep(): bool|string {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousStep(): bool|string {
    return StepEmailVerification::ID;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Verification code'),
      '#description' => $this->t('Enter the code received by email in order to finalize with your account creation.'),
      '#maxlength' => 50,
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $generator = new DefaultPasswordGenerator();
    $password = $generator->generate(20);
    $response = $this->openSolrAccount->createAccount($this->privateTempStore->get('email'), $form_state->getValue('code'), $password);
    if (!$response->isSuccess()) {
      $form_state->setErrorByName('code', $this->t('Failed to create your opensolr account with the following message: %message', [
        '%message' => OpenSolrErrors::getError($response->getResponseData()),
      ]));
    }
    else {
      // @todo Decide if we add extra check for the existence of the API key.
      $this->privateTempStore->set('api_key', str_replace('API_KEY:', '', $response->getResponseData()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->openSolrConfig->setEmail($this->privateTempStore->get('email'));
    $this->openSolrConfig->setApiKey($this->privateTempStore->get('api_key'));
    $this->clearPrivateTempStore();
    $this->messenger()->addStatus($this->t('Your opensolr account was successfully created. Select a Solr environment to finish your setup.'));
    $form_state->setRedirect('search_api_opensolr.autoconfigure_form');
  }

  /**
   * Clears the form data from the private temp store.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  protected function clearPrivateTempStore(): void {
    $keys = ['step', 'email', 'api_key'];
    foreach ($keys as $key) {
      $this->privateTempStore->delete($key);
    }
  }

}
