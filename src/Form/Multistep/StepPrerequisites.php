<?php

namespace Drupal\search_api_opensolr\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Handles the Prerequisites step in the multistep form.
 *
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepInterface
 * @see \Drupal\search_api_opensolr\Form\Multistep\MultistepStepBase
 */
class StepPrerequisites extends MultistepStepBase {

  const ID = 'prerequisites';

  /**
   * {@inheritdoc}
   */
  public function weight(): int {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): TranslatableMarkup {
    return $this->t('Prerequisites');
  }

  /**
   * {@inheritdoc}
   */
  public function getNextStep(): bool|string {
    return StepEmailVerification::ID;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreviousStep(): bool|string {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form['existing_account'] = [
      '#type' => 'radios',
      '#title' => $this->t('Do you have an <a href=":url" target="_blank">opensolr</a> account?', [
        ':url' => 'https://opensolr.com',
      ]),
      '#options' => [
        1 => $this->t('Yes'),
        0 => $this->t('No'),
      ],
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('existing_account')) {
      $this->messenger()->addStatus($this->t('If you already have an account, add your email and API Key here.'));
      $form_state->setRedirect('search_api_opensolr.opensolr_config_form');
    }
    else {
      $this->privateTempStore->set('step', $this->getNextStep());
      $form_state->setRebuild();
    }
  }

}
