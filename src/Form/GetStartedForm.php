<?php

namespace Drupal\search_api_opensolr\Form;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Drupal\search_api_opensolr\Form\Multistep\StepEmailVerification;
use Drupal\search_api_opensolr\Form\Multistep\StepPrerequisites;
use Drupal\search_api_opensolr\Form\Multistep\StepRegistration;
use Drupal\search_api_opensolr\Services\OpenSolrConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Get started" form for opensolr registration.
 */
class GetStartedForm extends FormBase {

  const TEMP_STORE = 'opensolr_get_started';

  /**
   * The private temp store for opensolr get started form.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $privateTempStore;

  /**
   * The opensolr config manager.
   *
   * @var \Drupal\search_api_opensolr\Services\OpenSolrConfig
   */
  protected OpenSolrConfig $openSolrConfig;

  /**
   * The class resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * The file url generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * An array containing the Multistep steps objects.
   *
   * @var array|\Drupal\search_api_opensolr\Form\Multistep\MultistepStepInterface[]
   */
  protected array $steps;

  public function __construct(PrivateTempStore $privateTempStore, OpenSolrConfig $openSolrConfig, ClassResolverInterface $classResolver, ThemeManagerInterface $themeManager, FileUrlGeneratorInterface $fileUrlGenerator, ModuleHandlerInterface $moduleHandler) {
    $this->privateTempStore = $privateTempStore;
    $this->openSolrConfig = $openSolrConfig;
    $this->classResolver = $classResolver;
    $this->themeManager = $themeManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->moduleHandler = $moduleHandler;
    $this->initSteps();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private')->get(GetStartedForm::TEMP_STORE),
      $container->get('search_api_opensolr.config'),
      $container->get('class_resolver'),
      $container->get('theme.manager'),
      $container->get('file_url_generator'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'opensolr_get_started';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->checkAccess()) {
      // @todo Haven't decided yet if we use form access or nah.
      return [];
    }
    // Initialize the steps & current step using the temp store.
    if (empty($this->privateTempStore->get('step'))) {
      $this->privateTempStore->set('step', StepPrerequisites::ID);
    }
    $step = $this->privateTempStore->get('step');
    $currentStep = $this->steps[$step];
    // Add the form wrapper with opensolr logo and additional CSS.
    $form['wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t("<div class='or-title'><div><img src=':logo' width='50'></div> <h3>opensolr Registration</h3></div>", [
        ':logo' => $this->getOpensolrLogo(),
      ]),
      '#open' => TRUE,
      '#attributes' => ['class' => ['opensolr-register-wrapper']],
    ];
    $this->attachLibrary($form);
    // Add the progress bar.
    $steps = array_map(function ($multistep) {
      return ['number' => $multistep->weight(), 'title' => $multistep->label()];
    }, $this->steps);
    $form['wrapper']['progress_bar'] = [
      '#theme' => 'opensolr_steps_progress_bar',
      '#active_step' => $currentStep->weight(),
      '#steps' => $steps,
    ];
    // Add the current step form.
    $currentStep->buildForm($form['wrapper'], $form_state);
    // Add action buttons.
    $form['wrapper']['actions'] = ['#type' => 'actions'];
    if ($currentStep->getPreviousStep()) {
      $form['wrapper']['actions']['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Back'),
        '#limit_validation_errors' => [],
        '#submit' => [[$this, 'submitFormBack']],
        '#opensolr_action' => 'back',
      ];
    }
    $form['wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $currentStep->getNextStep() ? $this->t('Next') : $this->t('Register'),
      '#button_type' => 'primary',
      '#opensolr_action' => 'submit',
    ];
    // Display info text about what is the purpose of the form.
    $form['wrapper']['info_text'] = [
      '#type' => 'inline_template',
      '#template' => '<div class="opensolr-info-note">{{ info_note }}</div>',
      '#context' => [
        'info_note' => $this->t('By completing this multistep form, you are creating a <a href="https://opensolr.com/faq/view/billing/34/solr-cloud-free-membership" target="_blank">free opensolr account</a> and generating the required related <a href=":url" target="_blank">Drupal configuration</a>.', [
          ':url' => Url::fromRoute('search_api_opensolr.opensolr_config_form')->toString(),
        ]),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    // Only validate "Next" actions.
    $action = $form_state->getTriggeringElement()['#opensolr_action'] ?? '';
    if ($action != 'submit') {
      return;
    }
    $step = $this->privateTempStore->get('step');
    $currentStep = $this->steps[$step];
    $currentStep->validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $step = $this->privateTempStore->get('step');
    $currentStep = $this->steps[$step];
    $currentStep->submitForm($form, $form_state);
  }

  /**
   * Form submission handler for the "Back" button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function submitFormBack(array &$form, FormStateInterface $form_state): void {
    $step = $this->privateTempStore->get('step');
    $currentStep = $this->steps[$step];
    if ($currentStep->getPreviousStep()) {
      $this->privateTempStore->set('step', $currentStep->getPreviousStep());
    }
  }

  /**
   * Initializes the multistep form steps.
   */
  protected function initSteps(): void {
    $this->steps = [
      StepPrerequisites::ID => $this->classResolver->getInstanceFromDefinition(StepPrerequisites::class),
      StepEmailVerification::ID => $this->classResolver->getInstanceFromDefinition(StepEmailVerification::class),
      StepRegistration::ID => $this->classResolver->getInstanceFromDefinition(StepRegistration::class),
    ];
  }

  /**
   * Verifies if there is an opensolr active configuration.
   *
   * @return bool
   *   Returns TRUE if the email and api_key exist, FALSE otherwise. Adds an
   *   error message if there is already an active configuration.
   */
  protected function checkAccess(): bool {
    if ($this->openSolrConfig->getEmail() && $this->openSolrConfig->getApiKey()) {
      $this->messenger()->addWarning($this->t('You cannot use the <em>Get started</em> form while already having an <a href=":url">active configuration</a>.', [
        ':url' => Url::fromRoute('search_api_opensolr.opensolr_config_form')->toString(),
      ]));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Attaches the admin library based on the current theme.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   */
  protected function attachLibrary(array &$form): void {
    if ($this->themeManager->getActiveTheme() == 'claro') {
      $form['#attached']['library'][] = 'search_api_opensolr/registration_claro';
    }
    else {
      $form['#attached']['library'][] = 'search_api_opensolr/registration';
    }
  }

  /**
   * Gets the relative path to the opensolr logo.
   *
   * @return string
   *   The opensolr logo relative URL.
   */
  protected function getOpensolrLogo(): string {
    $uri = $this->moduleHandler->getModule('search_api_opensolr')->getPath() . '/logo.png';
    return $this->fileUrlGenerator->generateString($uri);
  }

}
