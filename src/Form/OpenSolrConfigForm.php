<?php

namespace Drupal\search_api_opensolr\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\key\KeyRepositoryInterface;
use Drupal\search_api_opensolr\OpenSolrApi\Components\OpenSolrIndex;
use Drupal\search_api_opensolr\OpenSolrApi\OpenSolrException;
use Drupal\search_api_opensolr\OpenSolrErrors;
use Drupal\search_api_opensolr\Services\OpenSolrConfig;
use Drupal\search_api_opensolr\Traits\OpensolrKeyDependencyTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form builder for the opensolr configuration form.
 *
 * @package Drupal\search_api_opensolr\Form
 */
class OpenSolrConfigForm extends ConfigFormBase {
  use OpensolrKeyDependencyTrait;

  /**
   * OpenSolr Index api component.
   *
   * @var \Drupal\search_api_opensolr\OpenSolrApi\Components\OpenSolrIndex
   */
  protected $searchApiOpensolrClient;


  /**
   * The opensolr config service.
   *
   * @var \Drupal\search_api_opensolr\Services\OpenSolrConfig
   */
  protected OpenSolrConfig $openSolrConfig;

  /**
   * The key repository service if the module exists and is enabled.
   *
   * @var \Drupal\key\KeyRepositoryInterface|false
   */
  protected false|KeyRepositoryInterface $keyRepository;

  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, OpenSolrIndex $search_api_opensolr_index, OpenSolrConfig $openSolrConfig) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->searchApiOpensolrClient = $search_api_opensolr_index;
    $this->keyRepository = $this->keyRepositoryService();
    $this->openSolrConfig = $openSolrConfig;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('search_api_opensolr.client_index'),
      $container->get('search_api_opensolr.config'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['search_api_opensolr.opensolrconfig'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'opensolr_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('search_api_opensolr.opensolrconfig')->get('opensolr_credentials');

    $form['opensolr_credentials'] = [
      '#type' => 'details',
      '#title' => $this->t('Opensolr API Credentials'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['opensolr_credentials']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Your opensolr email.'),
      '#default_value' => $config['email'] ?? '',
      '#required' => TRUE,
    ];
    if ($this->keyRepository) {
      $form['opensolr_credentials']['#description'] = $this->t('The <a href="https://www.drupal.org/project/key" target="_blank">Key Module</a> is an optional integration. It can be uninstalled if you do not wish to use it anymore.');
      $form['opensolr_credentials']['api_key'] = [
        '#type' => 'key_select',
        '#title' => $this->t('Secret key'),
        '#required' => TRUE,
        '#description' => $this->t('Also, the key has to belong to the <strong>authentication</strong> group'),
        '#key_filters' => ['type_group' => 'authentication'],
        '#default_value' => $config['api_key'] ?? '',
      ];
      $form['opensolr_credentials']['api_key_raw'] = [
        '#type' => 'hidden',
      ];
    }
    else {
      $form['opensolr_credentials']['#description'] = $this->t('Optional integration with <a href="https://www.drupal.org/project/key" target="_blank">Key Module</a> is available if you wish to use it. Install it and reconfigure this form.');
      $form['opensolr_credentials']['api_key_raw'] = [
        '#type' => 'password',
        '#title' => $this->t('Secret key'),
        '#attributes' => [
          'class' => ['pass-show'],
        ],
        '#prefix' => '<div class="pass-show-wrapper">',
        '#suffix' => '</div>',
        '#required' => TRUE,
        '#description' => $this->t('The secret key can be retrieved from your opensolr account.'),
        '#default_value' => $config['api_key_raw'] ?? '',
      ];
      $form['opensolr_credentials']['api_key'] = [
        '#type' => 'hidden',
      ];
      $form['#attached']['library'][] = 'search_api_opensolr/admin-tweaks';
      $form['#attached']['drupalSettings']['opensolr_key'] = $config['api_key_raw'] ?? '';
    }

    $form = parent::buildForm($form, $form_state);
    $form['actions']['connection'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test connection'),
      '#submit' => ['::testConnectionSubmit'],
    ];

    return $form;
  }

  /**
   * Submission handler to test the opensolr connection.
   */
  public function testConnectionSubmit(array &$form, FormStateInterface $form_state) {
    $credentials = $form_state->getValue('opensolr_credentials');
    $credentials = $this->openSolrConfig->extractApiCredentials($credentials);
    // Make a test call to the API to make sure that the credentials are
    // correct. We keep this here for now because of fallback to previous
    // development.
    try {
      $result = $this->searchApiOpensolrClient->getIndexList($credentials);

      if (isset($result['status']) && $result['status'] === FALSE) {
        $this->messenger()->addError($this->t('The opensolr test connection failed with the message <em>@message</em>. Verify your credentials and try again.', [
          '@message' => OpenSolrErrors::getError($result['msg']) ?? '',
        ]));
      }
      else {
        // If we reached this point, it means that the test connection was
        // successful.
        $this->messenger()->addStatus($this->t('The test connection to OpenSolr services was successful.'));
      }
    }
    catch (OpenSolrException $exception) {
      $this->messenger()->addError($this->t('The opensolr test connection failed with the message <em>@message</em>. Make sure that you have <strong>at least one index</strong> available before testing the connection.<br> You can create one in your opensolr account or by using the <a href=":url">autoconfigure feature</a>.', [
        '@message' => OpenSolrErrors::getError($exception->getMessage()),
        ':url' => Url::fromRoute('search_api_opensolr.autoconfigure_form')->toString(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Save the configuration.
    $this->config('search_api_opensolr.opensolrconfig')
      ->set('opensolr_credentials', $form_state->getValue('opensolr_credentials'))
      ->save();
  }

}
