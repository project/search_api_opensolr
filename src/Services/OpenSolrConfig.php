<?php

namespace Drupal\search_api_opensolr\Services;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\key\Entity\Key;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\search_api_opensolr\Traits\OpensolrKeyDependencyTrait;

/**
 * Retrieves and processes the global opensolr configuration.
 */
class OpenSolrConfig {
  use OpensolrKeyDependencyTrait;

  /**
   * The opensolr configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * The key repository service if the key module is installed.
   *
   * @var false|\Drupal\key\KeyRepositoryInterface
   */
  protected false|KeyRepositoryInterface $keyRepositoryService;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->getEditable('search_api_opensolr.opensolrconfig');
    $this->keyRepositoryService = $this->keyRepositoryService();
  }

  /**
   * Sets the opensolr email used in API calls.
   *
   * @param string $email
   *   The opensolr email.
   */
  public function setEmail(string $email): void {
    $credentials = $this->config->get('opensolr_credentials');
    $credentials['email'] = $email;
    $this->config->set('opensolr_credentials', $credentials);
    $this->config->save();
  }

  /**
   * Gets the opensolr email from the Drupal configuration.
   *
   * @return string
   *   The opensolr email.
   */
  public function getEmail(): string {
    $config = $this->config->get('opensolr_credentials');
    return $config['email'] ?? '';
  }

  /**
   * Sets the opensolr API key.
   *
   * If the Key module exists, we generate a new default key with the API key as
   * value, otherwise we set the raw key in the configuration.
   *
   * @param string $apiKey
   *   The API key.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setApiKey(string $apiKey): void {
    $credentials = $this->config->get('opensolr_credentials');
    if ($this->keyRepositoryService) {
      $key = $this->createKey($apiKey);
      $credentials['api_key'] = $key->id();
    }
    else {
      $credentials['api_key_raw'] = $apiKey;
    }
    $this->config->set('opensolr_credentials', $credentials);
    $this->config->save();
  }

  /**
   * Gets the opensolr API key from the Drupal configuration.
   *
   * @return string
   *   The opensolr API key.
   */
  public function getApiKey(): string {
    $credentials = $this->getApiCredentials();
    return $credentials['api_key'] ?? '';
  }

  /**
   * Gets the API credentials from the stored configuration.
   *
   * @return array
   *   Returns an array with the email and api_key keys and values.
   */
  public function getApiCredentials(): array {
    return $this->extractApiCredentials($this->config->get('opensolr_credentials'));
  }

  /**
   * Extracts the API credentials from the given array.
   *
   * @param array $credentials
   *   An array of credentials that should contain the email, the api_key and
   *   the api_key_raw. The api_key is available when using the key module, the
   *   other one is used when key is not available.
   *
   * @return array
   *   Returns an array with the email and the api_key.
   */
  public function extractApiCredentials(array $credentials): array {
    if ($this->keyRepositoryService) {
      // Override with key credential if it exists.
      $key = $this->keyRepositoryService->getKey($credentials['api_key']);
      if ($key instanceof KeyInterface) {
        $credentials['api_key'] = $key->getKeyValue();
      }
    }
    else {
      $credentials['api_key'] = $credentials['api_key_raw'];
      unset($credentials['api_key_raw']);
    }
    return $credentials;
  }

  /**
   * Generates a key that contains the opensolr API key.
   *
   * @param string $value
   *   The opensolr API key.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\key\Entity\Key
   *   Returns the newly created Key object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createKey(string $value): EntityInterface|Key {
    $key = Key::create([
      'id' => uniqid('opensolr_'),
      'label' => 'opensolr auto key',
      'description' => 'Key generated automatically for opensolr configuration',
      'key_type' => 'authentication',
      'key_provider' => 'config',
      'key_provider_settings' => [
        'key_value' => $value,
      ],
      'key_input' => 'text_field',
    ]);
    $key->save();
    return $key;
  }

}
