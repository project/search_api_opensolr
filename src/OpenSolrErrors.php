<?php

namespace Drupal\search_api_opensolr;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Helper class for manipulating opensolr error codes.
 */
class OpenSolrErrors {

  /**
   * Transforms the given opensolr error code.
   *
   * @param string $errorCode
   *   The opensolr error code.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Returns the human-readable translation of the given opensolr error code.
   */
  public static function getError(string $errorCode): string|TranslatableMarkup {
    return match (TRUE) {
      $errorCode == 'SYSTEM_ERROR:INVALID_CODE_FOR_EMAIL' => t('The verification code is invalid'),
      $errorCode == 'SYSTEM_ERROR:USER_ALREADY_EXISTS' => t('The user already exists'),
      $errorCode == 'ERROR_AUTHENTICATION_FAILED' => t('Authentication failed'),
      str_starts_with($errorCode, 'SYSTEM_ERROR:CAN_NOT_RESEND_SO_FAST_WAIT:') => t('A request has already been made recently. Time to wait for a new attempt: @time', [
        '@time' => str_replace('SYSTEM_ERROR:CAN_NOT_RESEND_SO_FAST_WAIT:', '', $errorCode),
      ]),
      str_starts_with($errorCode, 'SYSTEM_ERROR:EMAIL_IS_TAKEN:') => t('The email @email is already taken.', [
        '@email' => str_replace('SYSTEM_ERROR:EMAIL_IS_TAKEN:', '', $errorCode),
      ]),
      $errorCode == 'ERROR_CANNOT_ADD_MORE_THAN_1_CORES' => t('Cannot add more than 1 core. Consider <a href="https://opensolr.com/pricing" target="_blank">upgrading</a> your opensolr account.'),
      default => $errorCode,
    };
  }

}
